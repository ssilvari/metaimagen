#ifndef SS_H
#define SS_H

#include<QDebug>
#include<QDir>
#include<QProcess>
#include<QSysInfo>
#include<QCoreApplication>

class SS: public QProcess {
    Q_OBJECT
public:
    SS();
private:
    QString kernel;
    QString workdir;
    QString command;
signals:
    void done();
public slots:
    bool runPipeline(QString);
    bool checkDockerInstallation();
};

#endif // SS_H
