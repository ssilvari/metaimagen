#include"SS.h"


int main(int argc, char *argv[])
{   
    QCoreApplication app(argc, argv);
    SS process;

    // Connect app to slot
    QObject::connect(&process, SIGNAL(done()), &app, SLOT(quit()), Qt::QueuedConnection);

    // Run pipeline
    if(process.checkDockerInstallation()) {
        process.runPipeline("test");
    }

    return app.exec();
}
