#include"SS.h"

SS::SS():QProcess () {
    kernel = QSysInfo::kernelType();
}


/* *****************************************
 * Methods
 * *****************************************/

// Run pipeline
bool SS::runPipeline(QString cmd) {
    // Download images
    qInfo() << "Downloading Images..." << cmd << endl;

    emit done();
    return true;
}

// Check Docker installation
bool SS::checkDockerInstallation() {
    QProcess docker;

    // Checks if Docker cannot be launched
    connect(&docker, &QProcess::errorOccurred, [=](QProcess::ProcessError error){
        // Print debug data
        qDebug() << error << endl;
        qDebug() << QProcess::exitCode();

        // Check if installed
        if (error == QProcess::FailedToStart){
            QString urlInfo = kernel == "linux" ? "https://get.docker.com/" : "https://docs.docker.com/docker-for-windows/install/";
            qInfo("It looks like Docker is not installed. Please, install docker. More info: %s",
                   urlInfo.toUtf8().constData());
            emit done();
            return false;
        }
    });

    // Try to launch process
    docker.setProgram("docker");
    docker.setArguments({"ps", "-a"});
    docker.start();
    docker.waitForFinished(10000);

    QString stdOut = QString::fromLocal8Bit(docker.readAllStandardOutput());
    QString stdErr = QString::fromLocal8Bit(docker.readAllStandardError());

    if (!stdOut.isEmpty()) qDebug() << "STDOUT: " << stdOut << endl;
    if (!stdErr.isEmpty()) qDebug() << "STERR: " << stdErr << endl ;

    if (stdErr.contains("daemon") && stdErr.contains("running") && stdErr.contains("connect")) {
        qInfo("It looks like Docker service is not running.");
        emit done();
        return false;
    } else if (stdOut.contains("CONTAINER ID")){
        return true;
    }
}
