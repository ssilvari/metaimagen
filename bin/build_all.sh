#!/bin/env bash
SERVER=$1

# Get current dir
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Build Standardization container
eval "bash ${CURRENT_DIR}/../centering_data/build.sh xx xx ${SERVER}"

# Build ADMM container
eval "bash ${CURRENT_DIR}/../ADMM/build.sh xx xx ${SERVER}"

# Build PLSR container
eval "bash ${CURRENT_DIR}/../PLSR/build.sh xx xx ${SERVER}"


# Push images
eval "docker push sssilvar/welford:latest"
eval "docker push sssilvar/admm_client:latest"
eval "docker push sssilvar/plsr:latest"

