#!/bin/bash

# Get client params
FS_DATASET=$1
GROUPFILE_FOLDER=$2
OUTPUT_FOLDER=$3
ID=$4
SERVER=${5-'https://olidmd.inria.fr:3300/'}

# Clear and run
clear

# Check arguments
if [ -z ${FS_DATASET} ] || [ -z ${GROUPFILE_FOLDER} ] || [ -z ${OUTPUT_FOLDER} ] || [ -z ${ID} ]; then
    echo -e "\n\n[  ERROR  ] Verify you inserted all the arguments\n\n"
    echo "DONE!"
    exit 1
fi

# If groupfile.csv included in GROUPFILE_FOLDER keep the dirname.
if [[ ${GROUPFILE_FOLDER} == *".csv" ]]; then
    GROUPFILE_FOLDER=$(dirname ${GROUPFILE_FOLDER})
fi

# Check if groupfile.csv exists
if [ ! -f $GROUPFILE_FOLDER"/groupfile.csv" ]; then
    echo -e "\n\n[  ERROR  ] File does not exist (${GROUPFILE_FOLDER}/groupfile.csv)"
    echo "DONE!"
    exit 1
fi


# Print basic information
echo "=========== MetaImaGen PipeLine ==========="
echo -e "\t- FreeSurfer Dataset Folder: "${FS_DATASET}
echo -e "\t- \"groupfile.csv\" folder: "${GROUPFILE_FOLDER}
echo -e "\t- Output Folder: "${OUTPUT_FOLDER}
echo -e "\t- Center ID: "${ID}

# Get current dir
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# 0. Run ENIGMA Shape pipeline (Subcortical)
SCRIPT=${CURRENT_DIR}"/../imaging/eshape/build.sh"
CMD="bash "${SCRIPT}" "${FS_DATASET}" "${OUTPUT_FOLDER}" "${GROUPFILE_FOLDER}" "$1
eval ${CMD}

# 1. Center data (Welford)
SCRIPT=${CURRENT_DIR}"/../centering_data/build.sh"
CMD="bash "${SCRIPT}" "${OUTPUT_FOLDER}" "${ID}" "${SERVER}
echo ${CMD}
eval ${CMD}

# 2. Distributed ADMM
SCRIPT=${CURRENT_DIR}"/../ADMM/build.sh"
CMD="bash "${SCRIPT}" "${OUTPUT_FOLDER}" "${ID}" "${SERVER}
echo ${CMD}
eval ${CMD}

# 3. PLSR
SCRIPT=${CURRENT_DIR}"/../PLSR/build.sh"
CMD="bash "${SCRIPT}" "${OUTPUT_FOLDER}" "${ID}" "${SERVER}
echo ${CMD}
eval ${CMD}


echo "DONE!"
