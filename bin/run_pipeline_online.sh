#!/bin/env bash

# Get client params
FS_DATASET=$1
GROUPFILE_FOLDER=$2
OUTPUT_FOLDER=$3
ID=$4

# Function for checking exit status
function check_exit_status {
    if [ $? -eq 1 ]
    then
        echo -e "\n [  ERROR  ] There was an error executing this task. Please check the output and contact support if needed."
        exit 1
    fi
}

# Clear and run
clear

# Check arguments
if [ -z ${FS_DATASET} ] || [ -z ${GROUPFILE_FOLDER} ] || [ -z ${OUTPUT_FOLDER} ] || [ -z ${ID} ]; then
    echo -e "\n\n[  ERROR  ] Verify you inserted all the arguments\n\n"
    echo "DONE!"
    exit 1
fi

# If groupfile.csv included in GROUPFILE_FOLDER keep the dirname.
if [[ ${GROUPFILE_FOLDER} == *".csv" ]]; then
    GROUPFILE_FOLDER=$(dirname ${GROUPFILE_FOLDER})
fi

# Check if groupfile.csv exists
if [ ! -f $GROUPFILE_FOLDER"/groupfile.csv" ]; then
    echo -e "\n\n[  ERROR  ] File does not exist (${GROUPFILE_FOLDER}/groupfile.csv)"
    echo "DONE!"
    exit 1
fi

# Print basic information
echo "=========== MetaImaGen Pipeline ==========="
echo -e "\t- FreeSurfer Dataset Folder: "${FS_DATASET}
echo -e "\t- \"groupfile.csv\" folder: "${GROUPFILE_FOLDER}
echo -e "\t- Output Folder: "${OUTPUT_FOLDER}
echo -e "\t- Center ID: "${ID}

# Get current dir
CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


# # 0.1 Run ENIGMA Shape pipeline (Subcortical)
# check_exit_status
# echo -e "\n\n[  OK  ] Running container for subcortical feature extraction."
# cmd="docker run --name enigma_shape_"${ID}" --rm -ti -v "${GROUPFILE_FOLDER}":/group/ -v "${FS_DATASET}":/input/ -v "${OUTPUT_FOLDER}":/output/ sssilvar/enigma_shape"
# echo $cmd
# eval $cmd

# # 0.1 Run cortical shape analysis
# check_exit_status
# echo -e "\n\n[  OK  ] Running container for cortical feature extraction."
# cmd="docker run --name cortical_shape_"${ID}" --rm -ti -v "${GROUPFILE_FOLDER}":/group/ -v "${FS_DATASET}":/input/ -v "${OUTPUT_FOLDER}":/output/ sssilvar/cortical_shape"
# echo $cmd
# eval $cmd

# 1. Center data (Distributed standardization)
check_exit_status
echo -e "\n\n[  OK  ] Running container for distributed data standardization."
CMD="docker run --name welford_"${ID}" --rm -ti -v "${OUTPUT_FOLDER}":/root/data/ -e \"CLIENT_ID=${ID}\" sssilvar/welford"
echo ${CMD}
eval ${CMD}

# 2. Correction from confounders
check_exit_status
echo -e "\n\n[  OK  ] Running container for distributed confounders correction."
CMD="docker run --name admm_client"${ID}" --rm -ti -v "${OUTPUT_FOLDER}":/root/data/ -e \"CLIENT_ID=${ID}\" sssilvar/admm_client"
echo ${CMD}
eval ${CMD}

# 3. PLSR
check_exit_status
echo -e "\n\n[  OK  ] Running container for distributed variance analysis (PLSR)."
CMD="docker run --name plsr_"${ID}" --rm -ti -v "${OUTPUT_FOLDER}":/root/data/ -e \"CLIENT_ID=${ID}\" sssilvar/plsr"
echo ${CMD}
eval ${CMD}