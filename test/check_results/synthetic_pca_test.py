import os
from os.path import join

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler

import seaborn as sns
import matplotlib.pyplot as plt

from PLSR import PLSR


if '__main__':
    os.system('clear')
    # Generate data
    n_features = 3457
    clusters = 4
    n_confounders = 7
    n_subjects = 100 * clusters
    n_centers = 100

    n_experiments = 1

    for i_exp in range(n_experiments):
        print('Generating data')
        # Generate data
        X = np.vstack([np.random.normal(i*4, size = [n_subjects // clusters, n_confounders]) for i in range(clusters)])        
        W = np.random.randn(n_confounders, n_features)
        Y = X.dot(W) + 0.2 * np.random.randn(n_subjects, n_features)
        Y = StandardScaler().fit_transform(Y)
        print(Y.shape)

        # Calculate global params (W and PC)
        W_re = np.linalg.inv(X.T.dot(X)).dot(X.T.dot(Y))
        # Plot W vs Y
        plt.scatter(x = W[0], y = W_re[0])
        plt.show()

        plsr = PLSR(X, Y)
        plsr.Initialize()
        plsr.EvaluateComponents()
        U, SU = plsr.ReturnComponents()
        print(X.shape)
        print(np.shape(U))

        # Split data
        X_split = np.split(X, n_centers, axis=0)
        Y_split = np.split(Y, n_centers, axis=0)
        
        # Calculate params (W and PC)
        U_c, SU_c = [], []
        for c in range(n_centers):
            # Assign center data
            X_c = X_split[c]
            Y_c = Y_split[c]

            # Compute PLSR
            plsr = PLSR(X_c, Y_c)
            plsr.Initialize()
            plsr.EvaluateComponents()
            U_t, SU_t = plsr.ReturnComponents()

            # Append to assembly the global
            U_c.append(U_t)
            SU_c.append(SU_t)

    # Reconstruct covriance matrix and
    # Do PCA
    U_c = np.vstack(U_c)
    SU_c = np.vstack(SU_c)

    # Calculate global and local PCA
    plsr = PLSR(U, SU)
    plsr.Initialize()
    plsr.EvaluateComponents()
    U_g, SU_g = plsr.ReturnComponents()

    plsr = PLSR(U_c, SU_c)
    plsr.Initialize()
    plsr.EvaluateComponents()
    U_l, SU_l = plsr.ReturnComponents()

    # Create and export DataFrame
    feats = ['F%d' % i for i in range(n_features)]
    comps = ['PC%d' % (c + 1) for c in range(len(U_g))]
    comps = comps + ['PC%d*' % (c + 1) for c in range(len(U_l))]

    print(SU_g.shape)
    print(SU_l.shape)
    data = np.vstack((SU_g, SU_l))
    df = pd.DataFrame(data = data.T, columns = comps, index = feats)

    # Save them
    csv_file = join(os.environ['HOME'], 'Downloads/components.csv')
    df.to_csv(csv_file, index=False)
