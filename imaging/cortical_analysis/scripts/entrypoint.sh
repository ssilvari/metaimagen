#!/usr/bin/env bash

# Load FreeSurfer Commands
source $FREESURFER_HOME/SetUpFreeSurfer.sh

# Run cortical pipeline extraction
echo "[  OK  ] Starting cortical feature extraction"
eval "python3 /root/scripts/pipeline.py"

eval "chmod -R 777 /output"

