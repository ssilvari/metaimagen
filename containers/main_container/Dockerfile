FROM ubuntu:bionic

# Set proxy (if necessary) --build-arg
ARG proxy
ENV http_proxy=$proxy

# Prevent dpkg errors
ENV TERM=xterm-256color

# Update OS and install necessary packages (but Freesurfer)
RUN apt-get -y update && \
    apt-get -y install perl bash python3 python3-pip

RUN pip3 install pandas>=0.22.0 requests>=2.18.4 numpy>=1.14.3
RUN ln -sf /bin/bash /bin/sh

# Install FreeSurfer
# ADD "https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/6.0.0/freesurfer-Linux-centos6_x86_64-stable-pub-v6.0.0.tar.gz" /
COPY freesurfer-Linux-centos6_x86_64-stable-pub-v6.0.0.tar.gz /
RUN mkdir /local && \
    tar -C /local -xzvf freesurfer-Linux-centos6_x86_64-stable-pub-v6.0.0.tar.gz && \
    rm /freesurfer-Linux-centos6_x86_64-stable-pub-v6.0.0.tar.gz
COPY license.txt /local/freesurfer

RUN echo "export FREESURFER_HOME=/local/freesurfer" >> ~/.bashrc && \
    echo "source \$FREESURFER_HOME/SetUpFreeSurfer.sh" >> ~/.bashrc

ENV FREESURFER_HOME=/local/freesurfer
RUN source "$FREESURFER_HOME/SetUpFreeSurfer.sh"

CMD [ "source", "~/.bashrc" ]